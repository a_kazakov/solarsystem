#include "Vector3D_double.h"

#include <cmath>

Vector3D_double::Vector3D_double(double x, double y, double z) :
    _x(x), _y(y), _z(z)
{
}

double Vector3D_double::length() const
{
    return std::sqrt(_x * _x + _y * _y + _z * _z);
}

Vector3D_double Vector3D_double::normalized() const
{
    double l = length();
    if (l < 1e-10) {
        return Vector3D_double(0.0, 1.0, 0.0);
    }
    return Vector3D_double(_x / l, _y / l, _z / l);
}

void Vector3D_double::normalize()
{
    *this = normalized();
}

double Vector3D_double::distanceToPoint(const Vector3D_double &v) const
{
    return (*this - v).length();
}

Vector3D_double Vector3D_double::normal(const Vector3D_double &a, const Vector3D_double &b)
{
    return Vector3D_double(a._y * b._z - a._z * b._y,
                           a._z * b._x - a._x * b._z,
                           a._x * b._y - a._y * b._x).normalized();
}

double Vector3D_double::dotProduct(const Vector3D_double &a, const Vector3D_double &b)
{
    return a._x * b._x + a._y * b._y + a._z * b._z;
}

QVector3D Vector3D_double::toQVector3D() const
{
    return QVector3D(_x, _y, _z);
}

