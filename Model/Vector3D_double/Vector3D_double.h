#pragma once

#include <QVector3D>

class Vector3D_double
{
public:
    Vector3D_double(double x = 0, double y = 0, double z = 0);
    inline double x() const {
        return _x;
    }
    inline double y() const {
        return _y;
    }
    inline double z() const {
        return _z;
    }
    inline void setX(double x) {
        _x = x;
    }
    inline void setY(double y) {
        _y = y;
    }
    inline void setZ(double z) {
        _z = z;
    }
    double length() const;
    Vector3D_double normalized() const;
    void normalize();
    double distanceToPoint(const Vector3D_double &v) const;
    inline const Vector3D_double &operator += (const Vector3D_double &v) {
        _x += v._x;
        _y += v._y;
        _z += v._z;
        return *this;
    }
    inline const Vector3D_double &operator -= (const Vector3D_double &v) {
        _x -= v._x;
        _y -= v._y;
        _z -= v._z;
        return *this;
    }
    inline const Vector3D_double &operator *= (double v) {
        _x *= v;
        _y *= v;
        _z *= v;
        return *this;
    }
    inline const Vector3D_double &operator /= (double v) {
        _x /= v;
        _y /= v;
        _z /= v;
        return *this;
    }
    static Vector3D_double normal(const Vector3D_double &a, const Vector3D_double &b);
    static double dotProduct(const Vector3D_double &a, const Vector3D_double &b);
    QVector3D toQVector3D() const;
    friend inline const Vector3D_double operator+(const Vector3D_double &v1, const Vector3D_double &v2);
    friend inline const Vector3D_double operator-(const Vector3D_double &v1, const Vector3D_double &v2);
    friend inline const Vector3D_double operator*(const Vector3D_double &vector, double factor);
    friend inline const Vector3D_double operator*(double factor, const Vector3D_double &vector);
protected:
    double _x, _y, _z;
};

inline const Vector3D_double operator+(const Vector3D_double &v1, const Vector3D_double &v2)
{
    return Vector3D_double(v1._x + v2._x, v1._y + v2._y, v1._z + v2._z);
}

inline const Vector3D_double operator-(const Vector3D_double &v1, const Vector3D_double &v2)
{
    return Vector3D_double(v1._x - v2._x, v1._y - v2._y, v1._z - v2._z);
}

inline const Vector3D_double operator*(double factor, const Vector3D_double &vector)
{
    return Vector3D_double(vector._x * factor, vector._y * factor, vector._z * factor);
}

inline const Vector3D_double operator*(const Vector3D_double &vector, double factor)
{
    return Vector3D_double(vector._x * factor, vector._y * factor, vector._z * factor);
}


