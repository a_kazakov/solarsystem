#pragma once

#include <Model/Sphere/Sphere.h>

class InvertedSphere : public Sphere
{
public:
    virtual void generateData();
};
