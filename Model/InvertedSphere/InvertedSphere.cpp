#define _USE_MATH_DEFINES
#include "InvertedSphere.h"

#include <iostream>
#include <cmath>

void InvertedSphere::generateData()
{
    if (_detalization == 0) {
        throw std::runtime_error("Detalization cannot be equal to zero. (Probably, you've forgotten to call InvertedSphere::setData')");
    }
    _n_vertexes = (2 * _detalization + 1) * (2 * _detalization + 1);
    _n_indexes  = 3 * 2 * 4 * _detalization * _detalization;

    _vertexes = new VertexData[_n_vertexes];
    _indexes = new GLuint[_n_indexes];

    int indCnt = 0;

    for (int i = -_detalization; i <= _detalization; ++i) {
        float rad = _radius;
        float ksi = static_cast<float>(i) / _detalization * M_PI / 2.0;
        float y = std::sinf(ksi) * rad;
        for (int j = 0; j <= 2 * _detalization; ++j) {
            float phi = static_cast<float>(j) / _detalization * M_PI;
            float x = std::cosf(ksi) * sinf(phi) * rad;
            float z = -std::cosf(ksi) * cosf(phi) * rad;
            _vertexes[(2 * _detalization + 1) * (i + _detalization) + j].position = QVector3D(x, y, z);
            _vertexes[(2 * _detalization + 1) * (i + _detalization) + j].normal = QVector3D(x, y, z);
            _vertexes[(2 * _detalization + 1) * (i + _detalization) + j].texCoord =
                                                QVector2D(phi / (2 * M_PI), (ksi + (M_PI / 2.0)) / M_PI);
            if (i != -_detalization && j != 2 * _detalization) {
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization) + (j + 1);
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization) + j;
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization - 1) + j;
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization) + (j + 1);
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization - 1) + j;
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization - 1) + (j + 1);
            }
        }
    }
}
