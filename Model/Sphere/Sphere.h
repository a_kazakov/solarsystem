#pragma once

#include <Model/Figure/Figure.h>

class Sphere : public Figure
{
public:
    Sphere();
    virtual void setData(double radius, int detalization);
    virtual ~Sphere();
protected:
    virtual void generateData();
    double _radius;
    int _detalization;
};
