#define _USE_MATH_DEFINES
#include "Sphere.h"

#include <iostream>
#include <cmath>

Sphere::Sphere() :
    _radius(0),
    _detalization(0),
    Figure(GL_TRIANGLES)
{

}

void Sphere::setData(double radius, int detalization)
{
    _radius = radius;
    _detalization = detalization;
}

Sphere::~Sphere()
{
    if (_vertexes) {
        delete [] _vertexes;
    }
    if (_indexes) {
        delete [] _indexes;
    }
}

void Sphere::generateData()
{
    if (_detalization == 0) {
        throw std::runtime_error("Detalization cannot be equal to zero. (Probably, you've forgotten to call Sphere::setData')");
    }
    _n_vertexes = (2 * _detalization + 1) * (2 * _detalization + 1);
    _n_indexes  = 3 * 2 * 4 * _detalization * _detalization;

    _vertexes = new VertexData[_n_vertexes];
    _indexes = new GLuint[_n_indexes];

    int indCnt = 0;

    for (int i = -_detalization; i <= _detalization; ++i) {
        float rad = _radius;
        float ksi = static_cast<float>(i) / _detalization * M_PI / 2.0;
        float y = std::sin(ksi) * rad;
        for (int j = 0; j <= 2 * _detalization; ++j) {
            float phi = static_cast<float>(j) / _detalization * M_PI;
            float x = std::cos(ksi) * sin(phi) * rad;
            float z = std::cos(ksi) * cos(phi) * rad;
            _vertexes[(2 * _detalization + 1) * (i + _detalization) + j].position = QVector3D(x, y, z);
            _vertexes[(2 * _detalization + 1) * (i + _detalization) + j].normal = QVector3D(x, y, z);
            _vertexes[(2 * _detalization + 1) * (i + _detalization) + j].texCoord =
                                                QVector2D(phi / (2 * M_PI), (ksi + (M_PI / 2.0)) / M_PI);
            if (i != -_detalization && j != 2 * _detalization) {
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization) + j;
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization - 1) + j;
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization) + (j + 1);
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization - 1) + j;
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization - 1) + (j + 1);
                _indexes[indCnt++] = (2 * _detalization + 1) * (i + _detalization) + (j + 1);
            }
        }
    }
}
