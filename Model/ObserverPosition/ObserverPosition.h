#pragma once

#include <QVector3D>

#include <Model/Vector3D_double/Vector3D_double.h>

#include <Serializer/ISerializer/ISerializer.h>

class ObserverPosition
{
public:
    ObserverPosition();
    void moveAhead(float step);
    void moveRight(float step);
    void turnUp(float angle);
    void turnRight(float angle);
    void moveTo(const Vector3D_double &point, float step, float rot_speed);
    Vector3D_double &getPosition();
    Vector3D_double getPosition() const;
    Vector3D_double getDirection() const;
    float getVertAngle() const;
    float getHorAngle() const;
    void serialize(ISerializer *s);
protected:
    float _angle_vert, _angle_hor;
    Vector3D_double _position;
private:
    mutable Vector3D_double _cached_direction;
    mutable bool _act_cached_direction;
};
