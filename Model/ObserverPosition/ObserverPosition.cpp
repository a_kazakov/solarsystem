#define _USE_MATH_DEFINES
#include <cmath>

#include <QDebug>

#include "ObserverPosition.h"

ObserverPosition::ObserverPosition() :
    _angle_vert(0.0f), _angle_hor(0.0f),
    _act_cached_direction(false),
    _position()
{

}

void ObserverPosition::moveAhead(float step)
{
    _position += step * getDirection();
}

void ObserverPosition::moveRight(float step)
{
    Vector3D_double dir = getDirection();
    Vector3D_double norm = Vector3D_double::normal(dir, dir + Vector3D_double(0.0f, 1.0f, 0.0f));
    _position += step * norm;
}

void ObserverPosition::turnUp(float angle)
{
    float bound = static_cast<float>(M_PI / 2.0 - M_PI / 100.0);
    _angle_vert = std::min<float>(std::max<float>(_angle_vert + angle, -bound), bound);
    _act_cached_direction = false;
}

void ObserverPosition::turnRight(float angle)
{
    _angle_hor += angle;
    float corr = static_cast<float>(M_PI * 2.0);
    if (_angle_hor < -M_PI) {
        _angle_hor += corr;
    }
    if (_angle_hor > M_PI) {
        _angle_hor -= corr;
    }
    _act_cached_direction = false;
}

void ObserverPosition::moveTo(const Vector3D_double &point, float step, float rot_speed)
{
    Vector3D_double move_vect = point -_position;
    float new_angle_hor = std::atan2f(move_vect.x(), -move_vect.z());
    float new_angle_vert = std::atan2f(move_vect.y(), std::sqrtf(move_vect.x() * move_vect.x() + move_vect.z() * move_vect.z()));
    while (new_angle_hor - _angle_hor > M_PI) {
        new_angle_hor -= static_cast<float>(2.0 * M_PI);
    }
    while (new_angle_hor - _angle_hor < -M_PI) {
        new_angle_hor += static_cast<float>(2.0 * M_PI);
    }
    turnRight(std::min(std::max(new_angle_hor - _angle_hor, -rot_speed), rot_speed));
    turnUp(std::min(std::max(new_angle_vert - _angle_vert, -rot_speed), rot_speed));
    if (Vector3D_double::dotProduct(move_vect.normalized(), getDirection().normalized()) > 0) {
        moveAhead(Vector3D_double::dotProduct(move_vect.normalized(), getDirection().normalized()) * step);
    }
}

float ObserverPosition::getVertAngle() const
{
    return _angle_vert / M_PI * 180.0;
}

float ObserverPosition::getHorAngle() const
{
    return _angle_hor / M_PI * 180.0;
}

void ObserverPosition::serialize(ISerializer *s)
{
    s->processItem(_angle_vert);
    s->processItem(_angle_hor);
    double x =_position.x();
    double y =_position.y();
    double z =_position.z();
    s->processItem(x);
    s->processItem(y);
    s->processItem(z);
    _position.setX(x);
    _position.setY(y);
    _position.setZ(z);
    _act_cached_direction = false;
}

Vector3D_double &ObserverPosition::getPosition()
{
    return _position;
}

Vector3D_double ObserverPosition::getPosition() const
{
    return _position;
}

Vector3D_double ObserverPosition::getDirection() const
{
    if (!_act_cached_direction) {
        _cached_direction = Vector3D_double(
            std::cos(_angle_vert) * std::sin(_angle_hor),
            std::sin(_angle_vert),
           -std::cos(_angle_vert) * std::cos(_angle_hor)
        );
        _cached_direction.normalize();
    }
    return _cached_direction;
}
