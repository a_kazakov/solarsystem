#pragma once

#include <QDateTime>
#include <QMatrix4x4>

#include <Model/ObserverPosition/ObserverPosition.h>

#include <Serializer/ISerializer/ISerializer.h>

struct SceneInfo
{
    SceneInfo();
    ObserverPosition observer;
    qreal angle;
    QDateTime time;
    int speed;
    bool use_log_scale;
    bool animating;
    void serialize(ISerializer *s);
};
