#include "SceneInfo.h"

SceneInfo::SceneInfo() :
    observer(),
    angle(45.0f),
    time(QDateTime::currentDateTimeUtc()),
    speed(1),
    use_log_scale(true),
    animating(true)
{

}

void SceneInfo::serialize(ISerializer *s)
{
    observer.serialize(s);
    s->processItem(angle);
    qint64 raw_time = time.toMSecsSinceEpoch();
    s->processItem(raw_time);
    time = QDateTime::fromMSecsSinceEpoch(raw_time);
    s->processItem(speed);
    s->processItem(use_log_scale);
    s->processItem(animating);
}
