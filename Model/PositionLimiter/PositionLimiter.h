#pragma once

#include <Model/Planet/Planet.h>
#include <Model/Vector3D_double/Vector3D_double.h>

#include <vector>

class PositionLimiter
{
public:
    PositionLimiter(double z_near);
    void addPlanet(Planet *p);
    void correctPosition(Vector3D_double &p, double sun_size);
private:
    std::vector<Planet *> _planets;
    double _z_near;
};
