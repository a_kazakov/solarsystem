#include "PositionLimiter.h"

const double PROHIBITED_AREA = 2.3;
const double SPACE_SIZE = 7.0;

PositionLimiter::PositionLimiter(double z_near) :
    _z_near(z_near)
{

}

void PositionLimiter::addPlanet(Planet *p)
{
    _planets.push_back(p);
}

void PositionLimiter::correctPosition(Vector3D_double &p, double sun_size)
{
    double max_rad = 0.0;
    for (Planet *it: _planets) {
        max_rad = std::max(max_rad, it->getPosition().length());
        double correction = (it->getRadius() + _z_near) * PROHIBITED_AREA - it->getPosition().distanceToPoint(p);
        if (correction > 0) {
            p += (p - it->getPosition()).normalized() * correction;
        }
    }
    double correction = p.length() - SPACE_SIZE * max_rad;
    if (correction > 0) {
        p += correction * (Vector3D_double() - p).normalized();
    }
    correction = (sun_size + _z_near) * PROHIBITED_AREA - p.length();
    if (correction > 0) {
        p += correction * p.normalized();
    }
}
