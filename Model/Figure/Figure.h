#pragma once

#include <Model/IFigure/IFigure.h>
#include <Model/ObserverPosition/ObserverPosition.h>
#include <Model/Vector3D_double/Vector3D_double.h>

#include <QVector2D>
#include <QVector3D>
#include <QtOpenGL>
#include <QOpenGLFunctions>

class Figure : public IFigure, protected QOpenGLFunctions
{
public:
    struct VertexData
    {
        QVector3D position;
        QVector2D texCoord;
        QVector3D normal;
    };
    Figure(GLenum draw_mode);
    virtual void setPosition(const Vector3D_double &newpos);
    virtual void rotateX(float angle);
    virtual void rotateY(float angle);
    virtual void rotateZ(float angle);
    virtual void resetRotation();
    virtual ~Figure();
    virtual void init(const QImage &texture, QGLWidget &context);
    virtual void draw(QOpenGLShaderProgram &, const QMatrix4x4 &projection, const Vector3D_double &camera_pos);
protected:
    // abstract
    virtual void generateData() = 0;

    QQuaternion _rotation;
    Vector3D_double _position;
    GLuint _vbo[2];

    VertexData *_vertexes;
    GLuint *_indexes;
    int _n_vertexes;
    int _n_indexes;

    GLuint _texture;
    QGLWidget *_context;

    bool _initialized;

    const GLenum _draw_mode;
};
