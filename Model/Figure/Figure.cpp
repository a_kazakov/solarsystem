#include "Figure.h"

Figure::Figure(GLenum draw_mode) :
    _draw_mode(draw_mode),
    _vertexes(0),
    _indexes(0),
    _initialized(false)
{

}

Figure::~Figure()
{
    if (_initialized) {
        _context->deleteTexture(_texture);
    }
}

void Figure::setPosition(const Vector3D_double &newpos)
{
    _position = newpos;
}

void Figure::rotateX(float angle)
{
    _rotation = QQuaternion::fromAxisAndAngle(1.0f, 0.0f, 0.0f, angle) * _rotation;
}

void Figure::rotateY(float angle)
{
    _rotation = QQuaternion::fromAxisAndAngle(0.0f, 1.0f, 0.0f, angle) * _rotation;
}

void Figure::rotateZ(float angle)
{
    _rotation = QQuaternion::fromAxisAndAngle(0.0f, 0.0f, 1.0f, angle) * _rotation;
}

void Figure::resetRotation()
{
    _rotation = QQuaternion();
}

void Figure::init(const QImage &texture, QGLWidget &context)
{
    initializeOpenGLFunctions();

    generateData();

    glGenBuffers(2, _vbo);

    glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, _n_vertexes * sizeof(VertexData), _vertexes, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, _n_indexes * sizeof(GLuint), _indexes, GL_STATIC_DRAW);

    // init texture

    glEnable(GL_TEXTURE_2D);

    _context = (&context);
    _texture = context.bindTexture(texture);

    // Set nearest filtering mode for texture minification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    // Set bilinear filtering mode for texture magnification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Wrap texture coordinates by repeating
    // f.ex. texture coordinate (1.1, 1.2) is same as (0.1, 0.2)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    _initialized = true;
}

void Figure::draw(QOpenGLShaderProgram &shader, const QMatrix4x4 &projection, const Vector3D_double &camera_pos)
{
    // Tell OpenGL which VBOs to use
    glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[1]);

    // Calculate model view transformation
    QMatrix4x4 matrix_mv;
    matrix_mv.translate((_position - camera_pos).toQVector3D());
    matrix_mv.rotate(_rotation);

    QMatrix4x4 matrix_rot;
    matrix_rot.rotate(_rotation);

    // Set modelview-projection matrix
    shader.setUniformValue( "mv_matrix", matrix_mv);
    shader.setUniformValue("mvp_matrix", projection * matrix_mv);
    shader.setUniformValue("rot_matrix", matrix_rot);

    // Offset for position
    quintptr offset = 0;

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertex_location = shader.attributeLocation("a_position");
    shader.enableAttributeArray(vertex_location);
    glVertexAttribPointer(vertex_location, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocation = shader.attributeLocation("a_texcoord");
    shader.enableAttributeArray(texcoordLocation);
    glVertexAttribPointer(texcoordLocation, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector2D);

    // Tell OpenGL programmable pipeline how to locate indexes data
    int normalVector = shader.attributeLocation("a_normal");
    shader.enableAttributeArray(normalVector);
    glVertexAttribPointer(normalVector, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Set texture
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _texture);
    shader.setUniformValue("texture", 0);

    // Execute the pipeline
    glDrawElements(_draw_mode, _n_indexes, GL_UNSIGNED_INT, 0);
}
