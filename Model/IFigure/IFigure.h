#pragma once

#include <QtOpenGL>

#include <Model/Vector3D_double/Vector3D_double.h>

class IFigure
{
public:
    virtual void setPosition(const Vector3D_double &newpos) = 0;
    virtual void rotateX(float angle) = 0;
    virtual void rotateY(float angle) = 0;
    virtual void rotateZ(float angle) = 0;
    virtual void resetRotation() = 0;
    virtual void init(const QImage &texture, QGLWidget &context) = 0;
    virtual void draw(QOpenGLShaderProgram &program, const QMatrix4x4 &projection, const Vector3D_double &camera_pos) = 0;
};
