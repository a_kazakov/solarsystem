/**
 * @file PlanetData.h
 * @author Artem Kazakov
 * @brief Contains class for holding and processing planet positions and sizes.
 */

#pragma once

#include <Model/Vector3D_double/Vector3D_double.h>

#include <QString>
#include <QVector3D>
#include <QDateTime>

#include <QJsonDocument>
#include <QJsonObject>

/**
 * @brief Class for holding and processing planet positions and sizes.
 * @author Artem Kazakov
 *
 * Neatly sketched from http://www.solar-system-explorer.com/scripts/ephemeris.js
 */

class PlanetData
{
public:
    /**
     * @brief Constructor
     * @param data infomation about orbits
     *
     * Example of data:
     * {
     *   "name": "Earth",
     *   "texture": "Textures/Earth.png",
     *   "initial": {
     *     "inner_rad": 6378,
     *     "sm_axis": 1.00000261,
     *     "ecc": 0.01671123,
     *     "incl": -0.00001531,
     *     "mean_long": 100.46457166,
     *     "per_arg": 102.93768193,
     *     "an_long": 0
     *   },
     *   "delta": {
     *     "sm_axis": 0.00000562,
     *     "ecc": -0.00004107,
     *     "incl": -0.01294668,
     *     "mean_long": 35999.37244981,
     *     "per_arg": 0.32327364,
     *     "an_long": 0.0
     *   }
     * }
     */
    PlanetData(const QJsonObject &data);
    /**
     * @brief Estimates position at given time
     * @param date Date in Julian day format
     * @param use_log_scale Determines whether use log scale or not
     * @return position in 3D space
     */
    Vector3D_double getPosition(const QDateTime &date, bool use_log_scale) const;
    /**
     * @brief Returns rasius of planet
     * @return Radius of planet
     */
    double getRadius() const;
    /**
     * @brief Estimate sidereal position
     * @return Current rotation state
     */
    double getRotationAngle(const QDateTime &time) const;
protected:
    /**
     * @brief Represents planet orbit according to Kepler.
     */
    struct KeplerOrbit {
        double a; //! semi-major axis
        double e; //! eccentricity
        double i; //! inclination
        double w; //! argument of periapsis
        double W; //! logtitude of the ascending node
        double l; //! mean longtituude
    };
    /**
     * @brief orbit data at zero time point
     */
    KeplerOrbit _initial_orbit;
    /**
     * @brief orbit alteration within a century
     */
    KeplerOrbit _delta_orbit;
    /**
     * @brief Radius of planet
     */
    double _radius;
    /**
     * @brief Sidereal rotation period
     */
    double _rotation_period;
    /**
     * @brief Calculates planet position from mean anomaly
     * @param eph Current ephemeris
     * @param m Current mean anomaly
     * @return Planet position in 3D space
     */
    Vector3D_double getEllipsePos(const KeplerOrbit &eph, double m, bool use_log_scale) const;
    /**
     * @brief calcuates ephemeris from date
     * @param date Date in Julian day format
     * @return
     */
    KeplerOrbit getEphemeris(double date) const;
    /**
     * @brief Converts QDate to Julian day format
     * @param date date to convert
     * @return Julian date
     */
    double toJulianDay(const QDateTime &date) const;
    /**
     * @brief Corrects one of ephemeris fields according to date
     * @param jdn Date in Julian day format
     * @param initial Value at zero time
     * @param rate_per_century Value alteration per century
     * @return Corrected value
     */
    double getEphemerisValue(double jdn, double initial, double rate_per_century) const;
};

