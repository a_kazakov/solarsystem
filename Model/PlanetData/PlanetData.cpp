#define _USE_MATH_DEFINES
#include <cmath>

#include "PlanetData.h"

#include <stdexcept>

#include <QDebug>

Vector3D_double PlanetData::getEllipsePos(const KeplerOrbit &eph, double M, bool use_log_scale) const
{
    double rad = use_log_scale ? 0.002 * std::log(1.0 + eph.a) : eph.a;
    double w = eph.w - eph.W;
    double E = M;
    double oE = M + 1.0;
    while (std::abs(E - oE) > 1e-10) {
        oE = E;
        E = M + eph.e * std::sin(E);
    }
    double x = rad * (std::cos(E) - eph.e);
    double y = rad * std::sqrt(1 - eph.e * eph.e) * std::sin(E);
    double sO = std::sin(eph.W);
    double cO = std::cos(eph.W);
    double sw = std::sin(w);
    double cw = std::cos(w);
    double cc = cw * cO;
    double ss = sw * sO;
    double sc = sw * cO;
    double cs = cw * sO;
    double ci = std::cos(eph.i);
    double si = std::sin(eph.i);
    double X  = (cc - ss * ci) * x + (-sc - cs * ci) * y;
    double Y  = (sw * si) * x + (cw * si) * y;
    double Z  = -(cs + sc * ci) * x - (-ss + cc * ci) * y;
    return Vector3D_double(X, Y, Z);
}

double PlanetData::getEphemerisValue(double jdn, double initial, double rate_per_century) const
{
    double centuries = (jdn - 2451543.5) / 36525.0;
    return initial + rate_per_century * centuries;
}

PlanetData::KeplerOrbit PlanetData::getEphemeris(double date) const
{
    KeplerOrbit eph;
    eph.a = getEphemerisValue(date, _initial_orbit.a, _delta_orbit.a);
    eph.e = getEphemerisValue(date, _initial_orbit.e, _delta_orbit.e);
    eph.i = getEphemerisValue(date, M_PI / 180.0 * _initial_orbit.i, M_PI / 180.0 * _delta_orbit.i);
    eph.l = getEphemerisValue(date, M_PI / 180.0 * _initial_orbit.l, M_PI / 180.0 * _delta_orbit.l);
    eph.w = getEphemerisValue(date, M_PI / 180.0 * _initial_orbit.w, M_PI / 180.0 * _delta_orbit.w);
    eph.W = getEphemerisValue(date, M_PI / 180.0 * _initial_orbit.W, M_PI / 180.0 * _delta_orbit.W);
    return eph;
}

double PlanetData::toJulianDay(const QDateTime &date) const
{
    return static_cast<double>(date.date().toJulianDay())
            + date.time().hour()   / (24.0)
            + date.time().minute() / (24.0 * 60.0)
            + date.time().second() / (24.0 * 60.0 * 60.0)
            + date.time().msec()   / (24.0 * 60.0 * 60.0 * 1000.0);
}

PlanetData::PlanetData(const QJsonObject &data)
{
    auto json_init = data["initial"].toObject();
    _rotation_period = json_init["rot_period"].toDouble();
    _radius = json_init["inner_rad"].toDouble() / 149597870.691;
    _initial_orbit.a = json_init["sm_axis"].toDouble();
    _initial_orbit.e = json_init["ecc"].toDouble();
    _initial_orbit.i = json_init["incl"].toDouble();
    _initial_orbit.w = json_init["per_arg"].toDouble();
    _initial_orbit.W = json_init["an_long"].toDouble();
    _initial_orbit.l = json_init["mean_long"].toDouble();
    auto json_delta = data["delta"].toObject();
    _delta_orbit.a = json_delta["sm_axis"].toDouble();
    _delta_orbit.e = json_delta["ecc"].toDouble();
    _delta_orbit.i = json_delta["incl"].toDouble();
    _delta_orbit.w = json_delta["per_arg"].toDouble();
    _delta_orbit.W = json_delta["an_long"].toDouble();
    _delta_orbit.l = json_delta["mean_long"].toDouble();
}

Vector3D_double PlanetData::getPosition(const QDateTime &date, bool use_log_scale) const
{
    auto eph = getEphemeris(toJulianDay(date));
    double M = eph.l - eph.w;
    // Modulus the mean anomaly so that -180 < M < 180
    double m = (M + M_PI) / (2.0 * M_PI);
    M = (m - std::floor(m)) * 2.0 * M_PI - M_PI;
    return getEllipsePos(eph, M, use_log_scale);
}

double PlanetData::getRadius() const
{
    return _radius;
}

double PlanetData::getRotationAngle(const QDateTime &time) const
{
    return -time.msecsTo(QDateTime(QDate(2000, 1, 1), QTime(0, 0))) / (1000.0 * 60.0 * 60.0 * 24.0 / 360.0) / _rotation_period;
}
