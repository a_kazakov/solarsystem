#include "PlanetManager.h"

PlanetManager::PlanetManager()
{

}

PlanetManager::~PlanetManager()
{
    for (Planet *it: _planets) {
        delete it;
    }
}

void PlanetManager::addPlanet(Planet *p)
{
    _planets.push_back(p);
}

void PlanetManager::setTime(const QDateTime &time)
{
    for (Planet *it: _planets) {
        it->setTime(time);
    }
}

void PlanetManager::drawAll(QOpenGLShaderProgram &shader, const QMatrix4x4 &projection, const Vector3D_double &camera_pos)
{
    for (Planet *it: _planets) {
        it->draw(shader, projection, camera_pos);
    }
}

void PlanetManager::setUseLogScale(bool val)
{
    for (Planet *it: _planets) {
        it->setUseLogScale(val);
    }
}

double PlanetManager::getMinDist(const Vector3D_double &curpos)
{
    double res = 1e+10;
    for (Planet *it: _planets) {
        res = std::min(res, curpos.distanceToPoint(it->getPosition()));
    }
    return res;
}

Vector3D_double PlanetManager::getPlanetCoord(int planet)
{
    return _planets[planet]->getPosition();
}
