#pragma once

#include <vector>

#include <QDateTime>

#include <Model/Planet/Planet.h>

class PlanetManager
{
public:
    PlanetManager();
    ~PlanetManager();
    void addPlanet(Planet *p);
    void setTime(const QDateTime &time);
    void drawAll(QOpenGLShaderProgram &shader, const QMatrix4x4 &projection, const Vector3D_double &camera_pos);
    void setUseLogScale(bool val);
    double getMinDist(const Vector3D_double &curpos);
    Vector3D_double getPlanetCoord(int planet);
protected:
    std::vector<Planet *> _planets;
};
