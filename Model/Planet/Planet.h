#pragma once

#include <Model/PlanetData/PlanetData.h>
#include <Model/Sphere/Sphere.h>

#include <QDateTime>
#include <QGLWidget>

class Planet : public Sphere
{
public:
    Planet(const QString &config_file, QGLWidget &context);
    ~Planet();
    void setTime(const QDateTime &time);
    void setUseLogScale(bool new_val);
    Vector3D_double getPosition();
    double getRadius();
protected:
    bool _use_log_scale;
    PlanetData *_planet_data;
};
