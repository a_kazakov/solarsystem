#include "Planet.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QTextStream>

Planet::Planet(const QString &config_file, QGLWidget &context) :
    _use_log_scale(false)
{
    QFile fp(config_file);
    if (!fp.open(QFile::ReadOnly | QFile::Text)) {
        throw std::runtime_error("Unable to open " + config_file.toStdString());
    }
    QTextStream ts(&fp);
    QString json_text = ts.readAll();
    auto json = QJsonDocument::fromJson(json_text.toLocal8Bit()).object();
    _planet_data = new PlanetData(json);
    Sphere::setData(_planet_data->getRadius(), 30);
    Sphere::init(QImage(json["texture"].toString()), context);
    setTime(QDateTime::currentDateTimeUtc());
}

Planet::~Planet()
{
    delete _planet_data;
}

void Planet::setTime(const QDateTime &time)
{
    setPosition(_planet_data->getPosition(time, _use_log_scale));
    resetRotation();
    rotateY(_planet_data->getRotationAngle(time));
}

Vector3D_double Planet::getPosition() {
    return _position;
}

double Planet::getRadius()
{
    return _planet_data->getRadius();
}

void Planet::setUseLogScale(bool new_val)
{
    _use_log_scale = new_val;
}
