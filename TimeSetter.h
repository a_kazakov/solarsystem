#pragma once

#include <QDialog>
#include <Model/SceneInfo/SceneInfo.h>

namespace Ui {
class TimeSetter;
}

class TimeSetter : public QDialog
{
    Q_OBJECT

public:
    explicit TimeSetter(QWidget *parent = 0);
    void setSceneInfoPtr(SceneInfo *si);
    void updateValues();
    ~TimeSetter();

private slots:
    void on_buttonBox_accepted();

private:
    SceneInfo *_si;
    Ui::TimeSetter *ui;
};
