#pragma once

#include <unordered_map>
#include <sstream>

#include <QMainWindow>
#include <QGLWidget>
#include <QtOpenGL>
#include <QTimer>
#include <QOpenGLFunctions>
#include <QBasicTimer>

#include <Model/Sphere/Sphere.h>
#include <Model/ObserverPosition/ObserverPosition.h>
#include <Model/Planet/Planet.h>
#include <Model/PlanetManager/PlanetManager.h>
#include <Model/InvertedSphere/InvertedSphere.h>
#include <Model/SceneInfo/SceneInfo.h>
#include <Model/Vector3D_double/Vector3D_double.h>
#include <Model/PositionLimiter/PositionLimiter.h>

#include <Serializer/JsonSerializer/JsonSerializer.h>
#include <Serializer/JsonDeserializer/JsonDeserializer.h>
#include <Serializer/BinarySerializer/BinarySerializer.h>
#include <Serializer/BinaryDeserializer/BinaryDeserializer.h>

#include "TimeSetter.h"

class MainWindow : public QGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setCustomDate(const QDateTime &date);
private:
    bool _mouse_camera_mode;

    QBasicTimer _timer;
    QOpenGLShaderProgram _phong_shader, _const_shader;
    QVector2D _latest_mouse_offset;
    qreal _aspect;
    QDateTime _latest_time;
    QMatrix4x4 _projection;

    Sphere *_sun, *_smallsun;
    InvertedSphere *_sky;

    PlanetManager *_planets;

    TimeSetter _date_picker;
    PositionLimiter _position_limiter;

    std::unordered_map<int, bool> _keys_pressed;

    SceneInfo _si;

    virtual void timerEvent(QTimerEvent *);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void wheelEvent(QWheelEvent *event);
    virtual void paintEvent(QPaintEvent *event);

    void centerMouse();
    QVector2D getMouseOffset();

    virtual void initializeGL();
    virtual void resizeGL(int new_width, int new_height);
    virtual void paintGL();

    float estimateCameraSpeed();

    void initShaders();
    void initTextures();

    void processError(const std::exception &ex);
    void updateProjection();
};
