#include "JsonSerializer.h"

#include <QJsonDocument>
#include <QFile>
#include <QTextStream>

#include <string>

JsonSerializer::JsonSerializer()
{
}

void JsonSerializer::processItem(bool &val)
{
    raw_array.append(val);
}

void JsonSerializer::processItem(int &val)
{
    raw_array.append(val);
}

void JsonSerializer::processItem(qint64 &val)
{
    QString str;
    QTextStream ts(&str);
    ts << val;
    processItem(str);
}

void JsonSerializer::processItem(float &val)
{
    raw_array.append(val);
}

void JsonSerializer::processItem(double &val)
{
    raw_array.append(val);
}

void JsonSerializer::processItem(QString &val)
{
    raw_array.append(val);
}

void JsonSerializer::saveToFile(const QString &filename)
{
    QFile f(filename);
    if (!f.open(QIODevice::WriteOnly | QIODevice::Text)) {
        throw std::runtime_error("Unable to save data to " + filename.toStdString());
    }
    f.write(QJsonDocument(raw_array).toJson());
    f.close();
}


