#pragma once

#include <Serializer/ISerializer/ISerializer.h>

#include <QJsonArray>
#include <QString>

class JsonSerializer : public ISerializer
{
public:
    JsonSerializer();
    virtual void processItem(bool &val);
    virtual void processItem(int &val);
    virtual void processItem(qint64 &val);
    virtual void processItem(float &val);
    virtual void processItem(double &val);
    virtual void processItem(QString &val);
    void saveToFile(const QString &filename);
private:
    QJsonArray raw_array;
};
