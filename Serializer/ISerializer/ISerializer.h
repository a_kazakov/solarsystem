#pragma once

#include <QString>

class ISerializer
{
public:
    virtual void processItem(bool &val) = 0;
    virtual void processItem(int &val) = 0;
    virtual void processItem(qint64 &val) = 0;
    virtual void processItem(float &val) = 0;
    virtual void processItem(double &val) = 0;
};
