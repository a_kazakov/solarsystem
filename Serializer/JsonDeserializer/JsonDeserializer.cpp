#include "JsonDeserializer.h"

#include <QFile>
#include <QJsonDocument>
#include <QTextStream>

#include <stdexcept>

JsonDeserializer::JsonDeserializer()
{
}

void JsonDeserializer::processItem(bool &val)
{
    if (ptr >= raw_data.size() || !raw_data[ptr].isBool()) {
        throw std::runtime_error("Unable to read a value from file (bool)");
    }
    val = raw_data[ptr++].toBool();
}

void JsonDeserializer::processItem(int &val)
{
    if (ptr >= raw_data.size() || !raw_data[ptr].isDouble()) {
        throw std::runtime_error("Unable to read a value from file (int)");
    }
    val = static_cast<int>(raw_data[ptr++].toDouble());
}

void JsonDeserializer::processItem(qint64 &val)
{
    QString str;
    processItem(str);
    QTextStream ts(&str);
    ts >> val;
}

void JsonDeserializer::processItem(float &val)
{
    if (ptr >= raw_data.size() || !raw_data[ptr].isDouble()) {
        throw std::runtime_error("Unable to read a value from file (float)");
    }
    val = static_cast<float>(raw_data[ptr++].toDouble());
}

void JsonDeserializer::processItem(double &val)
{
    if (ptr >= raw_data.size() || !raw_data[ptr].isDouble()) {
        throw std::runtime_error("Unable to read a value from file (double)");
    }
    val = raw_data[ptr++].toDouble();
}

void JsonDeserializer::processItem(QString &val)
{
    if (ptr >= raw_data.size() || !raw_data[ptr].isString()) {
        throw std::runtime_error("Unable to read a value from file (string)");
    }
    val = raw_data[ptr++].toString();
}

void JsonDeserializer::loadFromFile(const QString &filename)
{
    QFile f(filename);
    if (!f.open(QFile::ReadOnly | QFile::Text)) {
        throw std::runtime_error("Unable to open " + filename.toStdString());
    }
    auto json_text = f.readAll();
    raw_data = QJsonDocument::fromJson(json_text).array();
    f.close();
    ptr = 0;
}
