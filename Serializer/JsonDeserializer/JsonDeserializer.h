#pragma once

#include <Serializer/ISerializer/ISerializer.h>

#include <QJsonArray>
#include <QString>

class JsonDeserializer : public ISerializer
{
public:
    JsonDeserializer();
    virtual void processItem(bool &val);
    virtual void processItem(int &val);
    virtual void processItem(qint64 &val);
    virtual void processItem(float &val);
    virtual void processItem(double &val);
    virtual void processItem(QString &val);
    void loadFromFile(const QString &filename);
private:
    int ptr;
    QJsonArray raw_data;
};
