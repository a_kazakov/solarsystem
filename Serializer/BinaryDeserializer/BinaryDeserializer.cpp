#include "BinaryDeserializer.h"

#include <QFile>

BinaryDeserializer::BinaryDeserializer()
{
}

void BinaryDeserializer::processItem(bool &val)
{
    processSimpleItem(val);
}

void BinaryDeserializer::processItem(int &val)
{
    processSimpleItem(val);
}

void BinaryDeserializer::processItem(qint64 &val)
{
    processSimpleItem(val);
}

void BinaryDeserializer::processItem(float &val)
{
    processSimpleItem(val);
}

void BinaryDeserializer::processItem(double &val)
{
    processSimpleItem(val);
}

void BinaryDeserializer::loadFromFile(const QString &filename)
{
    QFile f(filename);
    if (!f.open(QFile::ReadOnly)) {
        throw std::runtime_error("Unable to open " + filename.toStdString());
    }
    raw_array = f.readAll();
    f.close();
    ptr = 0;
}
