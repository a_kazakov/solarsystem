#pragma once

#include <Serializer/ISerializer/ISerializer.h>

class BinaryDeserializer : public ISerializer
{
public:
    BinaryDeserializer();
    virtual void processItem(bool &val);
    virtual void processItem(int &val);
    virtual void processItem(qint64 &val);
    virtual void processItem(float &val);
    virtual void processItem(double &val);
    void loadFromFile(const QString &filename);
private:
    int ptr;
    QByteArray raw_array;

    template <typename T>
    void processSimpleItem(T &val) {
        if (static_cast<int>(ptr + sizeof(T)) > raw_array.size()) {
            throw std::exception("End of file reached while deserializing.");
        }
        val = *reinterpret_cast<const T *>(static_cast<const char *>(raw_array) + ptr);
        ptr += sizeof(T);
    }
};
