#include "BinarySerializer.h"

#include <QFile>

BinarySerializer::BinarySerializer()
{
}

void BinarySerializer::processItem(bool &val)
{
    processSimpleItem(val);
}

void BinarySerializer::processItem(int &val)
{
    processSimpleItem(val);
}

void BinarySerializer::processItem(qint64 &val)
{
    processSimpleItem(val);
}

void BinarySerializer::processItem(float &val)
{
    processSimpleItem(val);
}

void BinarySerializer::processItem(double &val)
{
    processSimpleItem(val);
}

void BinarySerializer::saveToFile(const QString &filename)
{
    QFile f(filename);
    if (!f.open(QIODevice::WriteOnly | QIODevice::Text)) {
        throw std::runtime_error("Unable to save data to " + filename.toStdString());
    }
    f.write(raw_array);
    f.close();
}
