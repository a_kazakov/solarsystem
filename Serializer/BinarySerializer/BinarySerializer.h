#pragma once

#include <Serializer/ISerializer/ISerializer.h>

#include <QByteArray>

class BinarySerializer : public ISerializer
{
public:
    BinarySerializer();
    virtual void processItem(bool &val);
    virtual void processItem(int &val);
    virtual void processItem(qint64 &val);
    virtual void processItem(float &val);
    virtual void processItem(double &val);
    void saveToFile(const QString &filename);
private:
    QByteArray raw_array;

    template <typename T>
    void processSimpleItem(T &val) {
        raw_array.append(reinterpret_cast<char *>(&val), sizeof(T));
    }
};
