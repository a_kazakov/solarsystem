#include "TimeSetter.h"
#include "ui_TimeSetter.h"

TimeSetter::TimeSetter(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimeSetter)
{
    ui->setupUi(this);
}

void TimeSetter::setSceneInfoPtr(SceneInfo *si)
{
    _si = si;
}

void TimeSetter::updateValues()
{
    ui->inpDate->setDateTime(_si->time);
    ui->inpSpeed->setValue(_si->speed);
}

TimeSetter::~TimeSetter()
{
    delete ui;
}

void TimeSetter::on_buttonBox_accepted()
{
    _si->time = ui->inpDate->dateTime();
    _si->speed = ui->inpSpeed->value();
}
