#include "mainwindow.h"

#include <algorithm>

const qreal Z_NEAR = 0.00001;
const qreal Z_FAR  = 30.0;

MainWindow::MainWindow(QWidget *parent)
    : QGLWidget(parent),
      _mouse_camera_mode(false),
      _latest_time(QDateTime::currentDateTimeUtc()),
      _position_limiter(Z_NEAR)
{
    _sun = new Sphere;
    _smallsun = new Sphere;
    _sun->setData(696342.0 / 149597870.691, 30);
    _smallsun->setData(0.0002f, 30);
    _sky = new InvertedSphere;
    _sky->setData(20.0f, 30);
    _date_picker.setSceneInfoPtr(&_si);
}

MainWindow::~MainWindow()
{
    delete _sun;
    delete _smallsun;
    delete _planets;
}

void MainWindow::setCustomDate(const QDateTime &date)
{
    _si.time = date;
}

void MainWindow::timerEvent(QTimerEvent *)
{
    try {
        auto curtime = QDateTime::currentDateTimeUtc();
        if (_si.animating) {
            _si.time = _si.time.addMSecs(_si.speed * _latest_time.msecsTo(curtime));
        }
        _planets->setUseLogScale(_si.use_log_scale);
        _planets->setTime(_si.time);
        double speed = estimateCameraSpeed();

        // move front
        if (_keys_pressed[Qt::Key_W] || _keys_pressed[Qt::Key_Up]) {
            _si.observer.moveAhead(speed);
        }
        // move back
        if (_keys_pressed[Qt::Key_S] || _keys_pressed[Qt::Key_Down]) {
            _si.observer.moveAhead(-speed);
        }
        // move left
        if (_keys_pressed[Qt::Key_A] || _keys_pressed[Qt::Key_Left]) {
            _si.observer.moveRight(-speed);
        }
        // move right
        if (_keys_pressed[Qt::Key_D] || _keys_pressed[Qt::Key_Right]) {
            _si.observer.moveRight(speed);
        }
        // camera down
        if (_keys_pressed[Qt::Key_K]) {
            _si.observer.turnUp(-0.05f);
        }
        // camera up
        if (_keys_pressed[Qt::Key_I]) {
            _si.observer.turnUp(0.05f);
        }
        // camera left
        if (_keys_pressed[Qt::Key_J]) {
            _si.observer.turnRight(-0.05f);
        }
        // camera right
        if (_keys_pressed[Qt::Key_L]) {
            _si.observer.turnRight(0.05f);
        }
        // camera to planet
        for (int i = 0; i < 9; ++i) {
            if (_keys_pressed['1' + i]) {
                _si.observer.moveTo(_planets->getPlanetCoord(i), estimateCameraSpeed(), std::min(50000, std::max(1, std::abs(_si.speed))) / 100.0f);
            }
        }
        // camera to sun
        if (_keys_pressed[Qt::Key_0]) {
            _si.observer.moveTo(Vector3D_double(), estimateCameraSpeed(), std::min(50000, std::max(1, std::abs(_si.speed))) / 100.0f);
        }
        if (_mouse_camera_mode) {
            auto offset = getMouseOffset();
            if (offset != QVector2D(0, 0)) {
                _si.observer.turnUp(0.01f * offset.y());
                _si.observer.turnRight(0.01f * offset.x());
            }
            centerMouse();
        }
        if (_si.use_log_scale) {
            _position_limiter.correctPosition(_si.observer.getPosition(), 0.0002);
        } else {
            _position_limiter.correctPosition(_si.observer.getPosition(), 696342.0 / 149597870.691);
        }
        _latest_time = curtime;
        paintGL();
    }
    catch (std::exception &ex) {
        processError(ex);
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    try {
        _keys_pressed[event->key()] = true;
    }
    catch (std::exception &ex) {
        processError(ex);
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    try {
        _keys_pressed[event->key()] = false;
        if (event->key() == Qt::Key_Space) {
            _si.animating = !_si.animating;
        }
        if (event->key() == Qt::Key_BracketRight) {
            _si.speed = std::min<int>(std::ceil(_si.speed + (std::abs(_si.speed) / 10.0) + 0.5), 10000000);
        }
        if (event->key() == Qt::Key_BracketLeft) {
            _si.speed = std::max<int>(std::floor(_si.speed - (std::abs(_si.speed) / 10.0) - 0.5), -10000000);
        }
        if (event->key() == Qt::Key_Q) {
            _si.use_log_scale = !_si.use_log_scale;
            _planets->setUseLogScale(_si.use_log_scale);
            if (!_si.use_log_scale) {
                _si.speed = 1;
            }
        }
        if (event->key() == Qt::Key_E) {
            _date_picker.updateValues();
            _date_picker.show();
        }
        if (event->key() == Qt::Key_C) { // load from json
            JsonDeserializer s;
            s.loadFromFile("save.json");
            _si.serialize(&s);
            updateProjection();
        }
        if (event->key() == Qt::Key_V) { // save to json
            JsonSerializer s;
            _si.serialize(&s);
            s.saveToFile("save.json");
        }
        if (event->key() == Qt::Key_B) { // load from binary
            BinaryDeserializer s;
            s.loadFromFile("save.bin");
            _si.serialize(&s);
            updateProjection();
        }
        if (event->key() == Qt::Key_N) { // save to binary
            BinarySerializer s;
            _si.serialize(&s);
            s.saveToFile("save.bin");
        }
    }
    catch (std::exception &ex) {
        processError(ex);
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    try {
        if (event->button() == Qt::RightButton) {
            _mouse_camera_mode = true;
            centerMouse();
        }
    }
    catch (std::exception &ex) {
        processError(ex);
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    try {
        if (event->button() == Qt::RightButton) {
            _mouse_camera_mode = false;
        }
    }
    catch (std::exception &ex) {
        processError(ex);
    }
}

void MainWindow::wheelEvent(QWheelEvent *event)
{
    try {
        _si.angle = std::min(std::max(static_cast<float>(_si.angle) - event->delta() / 100.0f, 10.0f), 120.0f);
        updateProjection();
    }
    catch (std::exception &ex) {
        processError(ex);
    }
}

void MainWindow::centerMouse()
{
    QPoint c(width() / 2, height() / 2);
    QCursor::setPos(mapToGlobal(c));
}

QVector2D MainWindow::getMouseOffset()
{
    QPoint c(width() / 2, height() / 2);
    auto pos = QCursor::pos() - mapToGlobal(c);
    _latest_mouse_offset = QVector2D(
                (3.0f * _latest_mouse_offset.x() + pos.x()) / 4.0f,
                (3.0f * _latest_mouse_offset.y() + pos.y()) / 4.0f
    );
    _latest_mouse_offset = QVector2D(
                std::abs(_latest_mouse_offset.x()) > 0.01f ? _latest_mouse_offset.x() : 0.0f,
                std::abs(_latest_mouse_offset.y()) > 0.01f ? _latest_mouse_offset.y() : 0.0f
    );
    return _latest_mouse_offset;
}

void MainWindow::initShaders() {
    // Overriding system locale until shaders are compiled
    setlocale(LC_NUMERIC, "C");
    bool ok;

    ok =       _phong_shader.addShaderFromSourceFile(QOpenGLShader::Vertex,   "Shaders/phong.vert");
    ok = ok && _phong_shader.addShaderFromSourceFile(QOpenGLShader::Fragment, "Shaders/phong.frag");
    ok = ok && _phong_shader.link();

    if (!ok) {
#ifndef _DEBUG
        exit(1);
#endif
    }

    ok =       _const_shader.addShaderFromSourceFile(QOpenGLShader::Vertex,   "Shaders/const.vert");
    ok = ok && _const_shader.addShaderFromSourceFile(QOpenGLShader::Fragment, "Shaders/const.frag");
    ok = ok && _const_shader.link();

    if (!ok) {
#ifndef _DEBUG
        exit(1);
#endif
    }

    // Restore system locale
    setlocale(LC_ALL, "");
}

void MainWindow::processError(const std::exception &ex)
{
    qDebug() << ex.what();
}

void MainWindow::initializeGL()
{
    try {
        initializeOpenGLFunctions();

        qglClearColor(Qt::black);

        qDebug() << "Initializing shaders...";
        initShaders();

        glEnable(GL_DEPTH_TEST);

        // Enable back face culling
        glEnable(GL_CULL_FACE);

        _sun->init(QImage("Textures/Sun.jpg"), *this);
        _sun->setPosition(Vector3D_double(0.0, 0.0, 0.0));
        _smallsun->init(QImage("Textures/Sun.jpg"), *this);
        _smallsun->setPosition(Vector3D_double(0.0, 0.0, 0.0));
        _sky->init(QImage("Textures/sky.jpg"), *this);

        _planets = new PlanetManager;

        Planet *p;
        p = new Planet("PlanetData/mercury.json", *this); _planets->addPlanet(p); _position_limiter.addPlanet(p);
        p = new Planet("PlanetData/venus.json",   *this); _planets->addPlanet(p); _position_limiter.addPlanet(p);
        p = new Planet("PlanetData/earth.json",   *this); _planets->addPlanet(p); _position_limiter.addPlanet(p);
        p = new Planet("PlanetData/mars.json",    *this); _planets->addPlanet(p); _position_limiter.addPlanet(p);
        p = new Planet("PlanetData/jupiter.json", *this); _planets->addPlanet(p); _position_limiter.addPlanet(p);
        p = new Planet("PlanetData/saturn.json",  *this); _planets->addPlanet(p); _position_limiter.addPlanet(p);
        p = new Planet("PlanetData/uranus.json",  *this); _planets->addPlanet(p); _position_limiter.addPlanet(p);
        p = new Planet("PlanetData/neptune.json", *this); _planets->addPlanet(p); _position_limiter.addPlanet(p);
        p = new Planet("PlanetData/pluto.json",   *this); _planets->addPlanet(p); _position_limiter.addPlanet(p);

        _planets->setUseLogScale(_si.use_log_scale);

        _timer.start(12, this);
        qDebug() << "Finished initialization.";
    }
    catch (std::exception &ex) {
        processError(ex);
    }
}

void MainWindow::updateProjection()
{
    _projection.setToIdentity();
    _projection.perspective(_si.angle, _aspect, Z_NEAR, Z_FAR);
}

void MainWindow::resizeGL(int new_width, int new_height)
{
    try {
        glViewport(0, 0, GLint(new_width), GLint(new_height));
        _aspect = qreal(new_width) / qreal(new_height > 0 ? new_height : 1);
        updateProjection();
    }
    catch (std::exception &ex) {
        processError(ex);
    }

}

void MainWindow::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
}

void MainWindow::paintGL()
{
    try {
        glClear(GL_COLOR_BUFFER_BIT);
        glClear(GL_DEPTH_BUFFER_BIT);

        Vector3D_double camera_pos = _si.observer.getPosition();

        QMatrix4x4 camera_rot;
        camera_rot.rotate(-_si.observer.getVertAngle(), 1.0f, 0.0f, 0.0f);
        camera_rot.rotate(_si.observer.getHorAngle(), 0.0f, 1.0f, 0.0f);

        QMatrix4x4 p_matrix = _projection * camera_rot;

        _const_shader.bind();

        _sky->setPosition(camera_pos);
        if (!_si.use_log_scale) {
            _sky->draw(_const_shader, p_matrix, camera_pos);
        }

        auto sun = _si.use_log_scale ? _smallsun : _sun;
        sun->draw(_const_shader, p_matrix, camera_pos);

        _phong_shader.bind();
        _phong_shader.setUniformValue("light_source", -camera_pos.toQVector3D());
        _planets->drawAll(_phong_shader, p_matrix, camera_pos);

        std::stringstream ss;
        ss << _si.time.toString("dd.MM.yyyy hh:mm:ss").toStdString()
           << " (speed: " << _si.speed << ")";
        setWindowTitle(QString::fromStdString(ss.str()));

        makeCurrent();
        swapBuffers();
    }
    catch (std::exception &ex) {
        processError(ex);
    }
}

float MainWindow::estimateCameraSpeed()
{
    double d = std::min(_planets->getMinDist(_si.observer.getPosition()), _si.observer.getPosition().length());
    double r = (d + 3.0 * std::pow(d, 3.0)) / 100.0 *
               (_si.animating && !_si.use_log_scale ? std::min(50.0, std::max(1.0, std::sqrt(std::abs(_si.speed)))) : 1);
    return std::min(d / 10.0, r);
}
