#-------------------------------------------------
#
# Project created by QtCreator 2013-11-12T11:09:55
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SolarSystem
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    Model/Sphere/Sphere.cpp \
    Model/Figure/Figure.cpp \
    Model/ObserverPosition/ObserverPosition.cpp \
    Model/PlanetData/PlanetData.cpp \
    Model/Planet/Planet.cpp \
    Model/PlanetManager/PlanetManager.cpp \
    Model/InvertedSphere/InvertedSphere.cpp \
    Model/SceneInfo/SceneInfo.cpp \
    Model/Vector3D_double/Vector3D_double.cpp \
    Model/PositionLimiter/PositionLimiter.cpp \
    TimeSetter.cpp \
    Serializer/JsonSerializer/JsonSerializer.cpp \
    Serializer/JsonDeserializer/JsonDeserializer.cpp \
    Serializer/BinarySerializer/BinarySerializer.cpp \
    Serializer/BinaryDeserializer/BinaryDeserializer.cpp

HEADERS  += mainwindow.h \
    Model/Sphere/Sphere.h \
    Model/IFigure/IFigure.h \
    Model/Figure/Figure.h \
    Model/ObserverPosition/ObserverPosition.h \
    Model/PlanetData/PlanetData.h \
    Model/Planet/Planet.h \
    Model/PlanetManager/PlanetManager.h \
    Model/InvertedSphere/InvertedSphere.h \
    Model/SceneInfo/SceneInfo.h \
    Model/Vector3D_double/Vector3D_double.h \
    Model/PositionLimiter/PositionLimiter.h \
    TimeSetter.h \
    Serializer/ISerializer/ISerializer.h \
    Serializer/JsonSerializer/JsonSerializer.h \
    Serializer/JsonDeserializer/JsonDeserializer.h \
    Serializer/BinarySerializer/BinarySerializer.h \
    Serializer/BinaryDeserializer/BinaryDeserializer.h

OTHER_FILES += \
    shaders/phong.frag \
    shaders/phong.vert \
    shaders/const.frag \
    shaders/const.vert

INCLUDEPATH += .

install_it.path = $$OUT_PWD
install_it.files += shaders
install_it.files += Textures
install_it.files += PlanetData

INSTALLS += install_it

FORMS += \
    TimeSetter.ui
