#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform sampler2D texture;

varying vec3 v_position;
varying vec2 v_texcoord;
varying vec3 v_normal;
varying vec3 v_light_source;

void main()
{
    vec4  diffColor = texture2D(texture, v_texcoord);
    const vec4  specColor = vec4(0.1, 0.1, 0.0, 1.0);
    const float specPower = 4.5;

    vec3 n2   = normalize(v_normal);
    vec3 l2   = normalize(v_light_source);
    vec3 v2   = normalize(v_position);
    vec3 r    = reflect (-v2, n2);
    vec4 diff = diffColor * min(0.1 + max(dot(n2, l2), 0.0), 1.0);
    vec4 spec = specColor * pow(max(dot(l2, r), 0.0), specPower);

    gl_FragColor = diff + spec;
}
