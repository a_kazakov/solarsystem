#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform mat4 mvp_matrix;
uniform mat4 mv_matrix;
uniform mat4 rot_matrix;

uniform vec3 light_source;

attribute vec4 a_position;
attribute vec2 a_texcoord;
attribute vec4 a_normal;

varying vec3 v_position;
varying vec2 v_texcoord;
varying vec3 v_normal;
varying vec3 v_light_source;

void main()
{
    gl_Position = mvp_matrix * a_position;
    vec3 p = vec3(mv_matrix * a_position);

    v_light_source = normalize(light_source - p);             // vector to light source
    v_position     = normalize(-p);                           // vector to the eye
    v_normal       = normalize(vec3(rot_matrix * a_normal));  // transformed normal

    v_texcoord     = a_texcoord;
}
